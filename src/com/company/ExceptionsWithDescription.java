package com.company;

public enum ExceptionsWithDescription {

    ERR001("Login Failed."),
    ERR002("Account has been locked.");

    String description;

    ExceptionsWithDescription(String description) {
        this.description = description;
    }
}
