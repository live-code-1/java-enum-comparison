package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        System.out.println("=== Exception class ===");
//        System.out.println(Exceptions.ERR001);
//        System.out.println(Exceptions.ERR001.name());
//        System.out.println(Exceptions.ERR001.ordinal());
//
//        System.out.println("=== Exception With Description class ===");
//        System.out.println(ExceptionsWithDescription.ERR001.name());
//        System.out.println(ExceptionsWithDescription.ERR001.description);

        System.out.println("=== Comparison ===");

        Scanner scanner = new Scanner(System.in);
        ExceptionsWithDescription errorCodeEnum;

        while (true) {
            try {
                System.out.print("Error Code: ");
                String errorCode = scanner.nextLine();

                //Convert string to enum
                errorCodeEnum = ExceptionsWithDescription.valueOf(errorCode.toUpperCase());
                break;
            } catch (IllegalArgumentException exception) {
                System.out.println("Invalid Error Code. Try Again");
            }
        }

        // #1 Compare by using CompareTo
        System.out.println("=== By using compareTo ===");
        System.out.println(errorCodeEnum.compareTo(ExceptionsWithDescription.ERR001));
        System.out.println(errorCodeEnum.compareTo(ExceptionsWithDescription.ERR002));

        // #2 Compare by using ==
        System.out.println("=== By using '==' ===");
        System.out.println(errorCodeEnum == ExceptionsWithDescription.ERR001);
        System.out.println(errorCodeEnum == ExceptionsWithDescription.ERR002);

        // #3 Compare by using .equal
        System.out.println("==== By using .equal ===");
        System.out.println(ExceptionsWithDescription.ERR001.equals(errorCodeEnum));
        System.out.println(ExceptionsWithDescription.ERR002.equals(errorCodeEnum));

        // #4 Compare by using .equal (Not null Safe)
//        System.out.println("==== By using .equal (Not Null Safe) ===");
//        errorCodeEnum = null;
//        System.out.println(errorCodeEnum.equals(ExceptionsWithDescription.ERR001));
//        System.out.println(errorCodeEnum.equals(ExceptionsWithDescription.ERR002));

        System.out.println("=== Perform Action ===");
        switch (errorCodeEnum) {

            case ERR001:
                System.out.println((ExceptionsWithDescription.ERR001.description));
                break;
            case ERR002:
                System.out.println((ExceptionsWithDescription.ERR002.description));
                break;
        }

    }
}




