# Java Enum Comparison | 2 Best ways to compare Enum (NULL SAFE) in JAVA

[![Java Enum Comparison](http://img.youtube.com/vi/v6EKQ3yRUgQ/0.jpg)](https://youtu.be/v6EKQ3yRUgQ)

*__Kindly Like, Subscribe and Share the YouTube Video__*

## Java Course - Lesson 4

What is Enum in Java? How to do java enum comparison? What is the best way to compare two java enum with NULL SAFE? In
this video, you will know 2 Best way java enum comparison method, and understand how to compare it in a safe way. Sure
that you can also know how to use java enum custom values to better explain the meaning of enum

*Feel free to comment and let us know what kind video you would like to watch*
